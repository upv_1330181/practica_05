package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Button button;
    Button button2;
    Button button3;
    Button button4;

    public static Intent intent =  null;
    public final static String EXTRA_MESSAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        intent = new Intent(this,DisplayImageActivity.class);

        button = (Button) findViewById(R.id.btnChangeImage);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_MESSAGE,"1");
                startActivity(intent);
            }
        });

        button2 = (Button) findViewById(R.id.btnChangeImage2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_MESSAGE,"2");
                startActivity(intent);
            }
        });

        button3 = (Button) findViewById(R.id.btnChangeImage3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_MESSAGE,"3");
                startActivity(intent);
            }
        });

        button4 = (Button) findViewById(R.id.btnChangeImage4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra(EXTRA_MESSAGE,"4");
                startActivity(intent);
            }
        });
    }
}